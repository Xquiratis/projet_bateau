#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint8_t toggle_port = 0; // Variable pour basculer l'état de la LED
volatile uint8_t counter_ovf = 0; //1 front montant = 250us avec prescaler de 1 CS0
volatile uint8_t counter_value = 0; //1 front montant = 250us avec prescaler de 1 CS0

void fonction_test_counter(uint8_t compteur_ovf,uint8_t compteur_valeur );
void WDT_off(void);

void enableInterrupts() {
  GIMSK |= _BV(PCIE);    // Activer les interruptions PCINT
  PCMSK |= _BV(PCINT2);  // Activer les interruptions sur la broche PB2
  sei();                 // Activer les interruptions globales
}

void general_setup() {
  pinMode(1, OUTPUT);
  digitalWrite(1,HIGH);

  enableInterrupts();

  // Configuration du Timer0 & Timer1
  TCCR1  &= 0b11110000;   // Arret du timer 1
  TCCR0B &= 0b11111000;   // Arret du timer 0
  TCNT0 = 0;              // Initialisation du compteur à 0 ; timer 0
  TCNT1 = 0;              // Initialisation du compteur à 0 ; timer 1
  TIMSK |= (1 << TOIE0);  // Activation de l'interruption Timer0 Overflow
  TIMSK |= (1 << TOIE1);  // Activation de l'interruption Timer1 Overflow

  //configuration & lancement watchdog
  WDTCR |= (0 << WDIE); //pas d'interruption
  WDTCR |= (1 << WDCE); //change enable watchdog => pour permettre de reset le watch dog
  WDTCR |= (1 << WDE); //enable watchdog
  WDTCR |= ((1 << WDP2) | (1 << WDP0)); //prescaler 64k => 500ms 
}

ISR (TIMER0_OVF_vect) {   // Routine d'interruption pour Timer0 Overflow
  counter_ovf += 1;
}
ISR (TIMER1_OVF_vect) {   // Routine d'interruption pour Timer1 Overflow
  //arret timer
  TCCR1  &= 0b11110000;
  TCNT1 = 0;
  //passage mode manu
  fonction_test_counter(0, 0);
}

ISR(PCINT0_vect) {
  //reset du watchdog
  //MCUSR = 0x00;
  //WDTCR = 0x00;
  //reset wathdog
  WDT_off();
  //relance watchdog
  WDTCR |= (1 << WDE); //enable watchdog
  WDTCR |= ((1 << WDP2) | (1 << WDP0)); //prescaler 64k => 500ms 

  if (digitalRead(2) == HIGH) {// Front montant,

    TCCR0B |= (1 << CS00);  // prescaler 256 => lancement timer
    //on stop le timer1
    TCCR1  &= 0b11110000;
    TCNT1 = 0;

  } else {// Front descendant,
    counter_value = TCNT0;
    TCCR0B &= 0b11111000;  // arrêter le timer
    TCNT0 = 0; 
    fonction_test_counter(counter_ovf, counter_value);
    counter_ovf = 0; //reset compteur de temps
  }
}

int main () {
  general_setup();
  while(1) {
  }
  return 0;
}

void fonction_test_counter(uint8_t compteur_ovf,uint8_t compteur_valeur)
{
  //MODE AUTO
  if (compteur_ovf == 5)//5 detection verds 1,3ms
  {
    //if((compteur_valeur >= 150)&&(compteur_valeur <= 200))//on augmente la précision entre 1,47ms et 1,52 ms
    if(compteur_valeur <= 250)//precision autour de 1,5ms
    {
      digitalWrite(1,LOW);
      //lancement du timer 1 pour durée trop longue
      TCCR1 |= ((1 << CS13) |  (1 << CS11)); //prescaler 512
    }
  }
  //MODE MANU
  else
  {
    digitalWrite(1,HIGH);
  }
}

void WDT_off(void)
{
  //_WDR();*/
  /* Clear WDRF in MCUSR */
  MCUSR = 0x00;
  /* Write logical one to WDCE and WDE */
  WDTCR |= (1<<WDCE) | (1<<WDE);
  /* Turn off WDT */
  WDTCR = 0x00;
}